#include "binary.h"

BinaryVector::BinaryVector()
{
    varNum = 0;
    len = 0;
    binaryVector = new unsigned int[len];
}

BinaryVector::BinaryVector(const int capacity, const int type)
{
    //умножение на 2^3 заменено сдвигом
    int uIntSize = sizeof(unsigned int) << 3;

    //функция от n переменных описывается набором из 2^n значений
    unsigned int initLen = 1 << capacity;
    varNum = capacity;

    // u_int имеет размер 4 байта. в 1 ячейку массива можно поместить 32 значения БФ
    //тогда необходимо поделить число значений на вместимость ячейки, чтобы определить
    //требуемую длину массива с округлением вверх
    // ceil возвращает наименьшее целое число, не меньшее передаваемого аргумента
    len = (int)ceil((float)initLen / (float)uIntSize);
    binaryVector = new unsigned int[len];
    switch (type)
    {
    //нуль-вектор
    case 0:
    {
        for (int i = 0; i < len; i++)
        {
            binaryVector[i] = 0;
        }
        break;
    }
    //единичный вектор
    case 1:
    {
        unsigned int vector = ~0;
        int i = 0;
        for (; i < (len - 1); i++)
        {
            binaryVector[i] = vector;
        }
        //заполняем оставшиеся n%32 = n & ((1 << n) - 1) элемента единицами
        binaryVector[i] = vector << (32 - (initLen & ((1 << 5) - 1)));
        break;
    }
    //генератор случайной функции
    case 2:
    {
        int i = 0;
        for (; i < (len - 1); i++)
        {
            binaryVector[i] = rand() - rand();
        }
        binaryVector[i] = rand() << (32 - (initLen & ((1 << 5) - 1)));
        break;
    }
    }
}

BinaryVector::BinaryVector(const BinaryVector &vector)
{
    varNum = vector.varNum;
    len = vector.len;
    binaryVector = new unsigned int[len];
    for (int i = 0; i < len; i++)
    {
        binaryVector[i] = vector.binaryVector[i];
    }
}

BinaryVector::BinaryVector(const string vector)
{
    int uIntSize = sizeof(unsigned int) << 3;
    int j = uIntSize - 1;
    unsigned int vectorLen = vector.length();
    unsigned int mask = 0;
    if ((vectorLen != 0) && (vectorLen & (vectorLen - 1)) == 0)
    {
        len = (int)ceil((float)vectorLen / (float)uIntSize);
        binaryVector = new unsigned int[len];

        varNum = 0;
        unsigned int k = vectorLen >> 1;
        while (k)
        {
            varNum++;
            k >>= 1;
        }
        for (int i = 0, k = 0; i < vectorLen; i++)
        {
            mask |= ((int)vector[i] - '0') << j;
            j--;
            if (j < 0 || (i == vectorLen - 1))
            {
                binaryVector[k] = mask;
                mask = 0;
                j = uIntSize - 1;
                k++;
            }
        }
    }
    else
    {
        binaryVector = new unsigned int[0];
        cout << "BinaryVector::constructor: varNum != 2^n\n";
    }
}

BinaryVector::~BinaryVector()
{
    delete[] this->binaryVector;
}

BinaryVector &BinaryVector::operator=(const BinaryVector &vector)
{
    if (this != &vector)
    {
        delete[] binaryVector;
        varNum = vector.varNum;
        len = vector.len;
        binaryVector = new unsigned int[len];
        for (int i = 0; i < len; i++)
        {
            binaryVector[i] = vector.binaryVector[i];
        }
    }
    return *this;
}

bool BinaryVector::operator==(BinaryVector &vector)
{
    if (varNum != vector.varNum)
    {
        return false;
    }
    for (int i = 0; i < len; i++)
    {
        if (binaryVector[i] == vector.binaryVector[i])
        {
            continue;
        }
        else
        {
            return false;
        }
    }
    return true;
}

ostream &operator<<(ostream &os, const BinaryVector &vector)
{
    int uIntSize = sizeof(unsigned int) << 3;
    int index;
    unsigned int mask = 0;
    unsigned int vectorLen = 1 << vector.varNum;

    mask |= 1 << (uIntSize - 1);

    for (int i = 0; i < vectorLen; i++)
    {
        index = (int)floor((float)i / (float)uIntSize);
        if (vector.binaryVector[index] & mask)
        {
            os << '1';
        }
        else
        {
            os << '0';
        }
        mask >>= 1;
        if (mask == 0)
        {
            mask |= 1 << (uIntSize - 1);
        }
    }
    return os;
}

int BinaryVector::weight()
{
    BinaryVector vector = *this;
    int weight = 0;
    for (int i = 0; i < vector.len; i++)
    {
        int x = vector.binaryVector[i];
        x -= (x >> 1) & 0x55555555;
        x = (x & 0x33333333) + ((x >> 2) & 0x33333333);
        x = (x + (x >> 4)) & 0x0F0F0F0F;
        x += x >> 8;
        x += x >> 16;
        weight += x & 0x3F;
    }
    return weight;
}

//функция проверки генератора случайной функции
float BinaryVector::onesFraction()
{
    float k = (float)this->weight() / (float)(1 << this->varNum);
    return k;
}

//Преобразование Мебиуса
BinaryVector BinaryVector::mebius()
{
    BinaryVector vector = *this;
    // check if varNum < 5, then works with only 32 bits
    (vector.varNum <= 5) ? toMebius32bits(0, vector.varNum, vector.binaryVector) : toMebius(0, vector.len, vector.binaryVector);
    return vector;
}

//Преобразование Мебиуса 32битного вектора
void BinaryVector::toMebius32bits(int i, int stepNum, unsigned int vector[])
{
    unsigned int mask[5] = {
        0b01010101010101010101010101010101,
        0b00110011001100110011001100110011,
        0b00001111000011110000111100001111,
        0b00000000111111110000000011111111,
        0b00000000000000001111111111111111};
    unsigned int shift = 1;

    for (int j = 0; j < stepNum; j++, shift <<= 1)
    {
        vector[i] ^= (vector[i] >> shift) & mask[j];
    }
}
/**
 *Рекурсивная функция вычисления преобразования Мебиуса
 *Выполнена по учебному алгоритму с расширением до 5 переменных
 *При передаче аргументом массива из 1 элемента, выполняется преобразование
 *над 32 битным вектором, работа функции завершается
 *В противном случае, вычисляется индекс середины массива,
 *количество шагов на данном этапе рекурсии.
 *Для каждой половины массива выполняем рекурсивный вызов
 *Последним действием рекурсии выполняем XOR первой половины со второй
 */
void BinaryVector::toMebius(int start, int end, unsigned int vector[])
{
    if (start == end - 1)
    {
        toMebius32bits(start, 5, vector);
        return;
    }
    int mid = ((start + end) >> 1);
    int stepNum = ((end - start) >> 1);
    toMebius(start, mid, vector);
    toMebius(mid, end, vector);

    for (int i = 0; i < stepNum; i++)
    {
        vector[mid + i] ^= vector[start + i];
    }
}

/**
 * Построение АНФ
 * 1) Выполняем преобразование Мебиуса
 * 2) Если при прохождении по массиву встречаем 1, включаем номер её бита в anf
 */
list<unsigned int> BinaryVector::ANF()
{
    BinaryVector vector = mebius();
    list<unsigned int> anf;
    unsigned int index = 0;

    for (int i = 0; i < len; i++)
    {
        for (int j = sizeof(unsigned int) * 8 - 1; j >= 0; j--, index++)
        {
            if ((vector.binaryVector[i] >> j) & 1 == 1)
            {
                anf.push_back(index);
            }
        }
    }
    return anf;
}

//построение и вывод АНФ с красивыми ⊕
void BinaryVector::printANF()
{
    string res;
    list<unsigned int> ANF0 = ANF();
    int index;
    for (auto i = ANF0.begin(); i != ANF0.end(); i++)
    {
        if (*(i) == 0)
        {
            res += "1⊕";
            continue;
        }
        index = varNum;
        for (int j = 0; j < varNum; j++)
        {
            if ((*(i) >> j) & 1 == 1)
            {
                res = res + "x" + to_string(index);
            }
            index--;
        }
        res += "⊕";
    }
    res = res.substr(0, res.size() - 3);
    cout << res << endl;
}

// BinaryVector BinaryVector::findAnnihilators(int deg)
// {
//     BinaryVector vector = *this;
//     int i, k;
//     for ()
// }

//проверка конструкторов: 1, от строки
void task1()
{
    cout << endl
         << "__constructor_of_ones__" << endl;
    for (int i = 2; i < 8; i++)
    {
        BinaryVector vector0(i, 1);
        cout << "w(" << vector0 << ") = " << vector0.weight() << endl;
    }

    cout << endl
         << "__constructor_of_string__" << endl;
    BinaryVector vector0("01111110000000000010000000000000");
    BinaryVector vector1("01111110");
    cout << "w(" << vector0 << ") = " << vector0.weight() << endl;
    cout << "w(" << vector1 << ") = " << vector1.weight() << endl;
}

//проверка генератора случайной функции
void task2()
{
    cout << endl
         << "__check_rand_generator__" << endl;
    for (int i = 2; i < 32; i++)
    {
        BinaryVector vector3(i, 2);
        cout << "n = " << vector3.varNum << "  w = " << vector3.weight();
        cout << "  k = w/2^n = " << vector3.onesFraction() << endl;
    }
}

//проверка вычисления преобразования мебиуса
void task3()
{
    cout << endl
         << "__check_Mebius__" << endl;
    BinaryVector vector0("11000110");
    cout << "n = 3  µ(" << vector0 << ") = " << vector0.mebius() << endl;
    cout << "µ(µ(f) = " << vector0.mebius() << endl;

    for (int i = 2; i < 7; i++)
    {
        BinaryVector vector(i, 2);
        cout << endl
             << "n = " << vector.varNum << "  µ(" << vector << ") = ";
        BinaryVector m = vector.mebius();
        BinaryVector m2 = m.mebius();
        cout << m << endl
             << "µ(µ(f) = " << m2 << "  : ";
        (vector == m2) ? cout << "true" << endl : cout << "false" << endl;
    }
    for (int i = 7; i < 25; i++)
    {
        BinaryVector vector(i, 2);
        cout << "n = " << vector.varNum;
        BinaryVector m = vector.mebius();
        BinaryVector m2 = m.mebius();

        cout << "µ(µ(f) : ";
        (vector == m2) ? cout << "true" << endl : cout << "false" << endl;
    }
}

//проверка вычисления АНФ по функции Мебиуса
void task4()
{
    cout << endl
         << "__check_ANF__" << endl;
    for (int i = 2; i < 6; i++)
    {
        BinaryVector vector(i, 2);
        cout << "ANF(" << vector << ") = ";
        vector.printANF();
    }
}

//функция вычисляет кол-во столбцов в таблице для нахождения AN(f)
// int countColumn(int varNum)
// {

// }

// функция нахождения числа сочетаний из n по k
int countComb(int n, int k)
{
    if (k == 0 || k == n)
        return 1;
    return countComb(n - 1, k - 1) * n / k;
}

/**
 * функция считает число сочетаний без повторений из n 
 * по 2..n элементов
 * из n по 1 не считаем, т.к. аннигилятором не может быть нуль-столбец
 */
int countAllComb(int n)
{
    int combNum = 0;
    for (int i = 2; i <= n; i++)
        combNum += countComb(n, i);
    return combNum;
}

// функция генерации следующей подстановки
bool generateNextCombination(int* temp, int n, int k)
{
    for (int i = k - 1; i >= 0; i--) {
        if (temp[i] < n - k + i + 1)
        {   
            temp[i]++;
            for (int j = i + 1; j < k; j++)
                temp[j] = temp[j-1] + 1;
            return true;
        }
    }
    return false;
}

// функция генерации num сочетаний без повторений из n по k
int** generateCombination(int n, int k, int num)
{
    int **comb = new int*[num]{};
    comb[0] = new int [k];
    //вычисляем первое сочетание, f.e. 12345
    for (int i = 0; i < k; i++)
        comb[0][i] = i + 1;
    for (int i = 1; i < num; i++) {
        comb[i] = new int[k];
        int *temp = new int[k];
        for (int j = 0; j < k; j++)
            temp[j] = comb[i-1][j];
        if (generateNextCombination(temp, n, k)) {
            for (int j = 0; j < k; j++)
                comb[i][j] = temp[j];
            delete [] temp;
            continue;
        }
        delete [] temp;
        break;
    }
    return comb;
}

/**
 * функция генерации всех сочетаний без повторений для номеров столбцов
 * вызов: string* comb = generateCombination(colNum);
 * На выходе: двумерный массив сочетаний:
 * [0]: [0][1][3][4]
 * [1]: [0][2][3][5]
 * ...
 * [combNum-1]: [combNum-4][combNum-3][combNum-2][combNum-1]
 */
int** generateAllCombination(int combNum, int colNum, int* len)
{
    int **comb = new int*[combNum]{};
    int iterator = 0;
    for (int i = 2; i <= colNum; i++) {
        int num = countComb(colNum, i);
        int** temp = generateCombination(colNum, i, num);
        for (int j = 0; j < num; j++, iterator++) {
             //выделяем память для сочетания из n по i элементов
            comb[iterator] = new int[i];
            for (int l = 0; l < i; l++)
                comb[iterator][l] = temp[j][l];
            len[iterator] = i;
        }
        for (int j = 0; j < num; j++) 
            delete [] temp[j];
    }
    return comb;
}

Timer::Timer() {}

Timer::~Timer() {}

void Timer::Start()
{
     startTime = time(0);
}

double Timer::Finish()
{
     time_t finTime = time(0);
     double diff = difftime(finTime, startTime);
     return diff;
}

//проверка времени выполнения преобразования Мебиуса для функции от 31 переменной
void task5()
{
    cout << "__chech_Mebius(31)__" << endl;
    BinaryVector vector0(31, 2);
    Timer t;
    t.Start();
    vector0.mebius();
    cout << "Timer = " << t.Finish() << " sec " << endl;
}

int main()
{
    srand(static_cast<unsigned int>(time(0)));
    task5();
}