#ifndef CRYPTOGRAPHIC_BOOLEAN_FUNCTIONS_BINARY_H
#define CRYPTOGRAPHIC_BOOLEAN_FUNCTIONS_BINARY_H

#include <iostream>
#include <ctime>
#include <string>
#include <cmath>
#include <list>

using namespace std;

class BinaryVector {
public:
    //varNum - число переменных, до 31
    unsigned int varNum;
    //len - длина массива binaryVector
    unsigned int len;
    unsigned int *binaryVector;

    BinaryVector();
    BinaryVector(const int capacity, const int type);
    BinaryVector(const BinaryVector &vector);
    BinaryVector(const string vector);
    ~BinaryVector();

    BinaryVector &operator= (const BinaryVector &vector);
    bool operator==(BinaryVector &vector);
    friend ostream &operator<< (ostream &os, const BinaryVector &vector);

    //возвращает вес вектора
    int weight();
    //функция проверки генератора случайной функции
    float onesFraction();

    BinaryVector mebius();
    void toMebius(int, int, unsigned int[]);
    void toMebius32bits(int i, int stepNum, unsigned int vector[]);
    
    list<unsigned int> ANF();
    void printANF();

    BinaryVector findAnnihilators(int deg);
};

class Timer
{
private:
    time_t startTime;

public:
    Timer();
    ~Timer();

    void Start();
    double Finish();
};
#endif //CRYPTOGRAPHIC_BOOLEAN_FUNCTIONS_BINARY_H
